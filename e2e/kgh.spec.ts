import type { MdSwitch } from '@material/web/switch/switch.js';
import type { KghDonation } from '../src/components/kgh-donation.js';

import { test, expect } from '@playwright/test';

import { KGHPage } from './KGHPage.model';
import { MdOutlinedTextField } from '@material/web/textfield/outlined-text-field.js';
import { sleep } from './sleep.js';

test.describe('Kupat Givat HaMivtar Site', () => {
  let kgh: KGHPage;
  /**
   * IDs of (deep) elements that make up the form
   */
  const formIds = [
    'anonymous',
    'email',
    'message',
    'monthly-field',
    'name',
    'other-amount',
    'phone',
    'stripe',
    'submit',
  ];

  test.beforeEach(async ({ page, browserName }) => {
    kgh = new KGHPage(page, browserName, { log: false });
    await kgh.navigate();
  });

  test('shows first step', async ({ page, browserName }) => {
    const amounts = await page.$$('#amount li');
    expect(amounts.length).toBe(6);
    expect(await page.isVisible('#other-amount')).toBeTruthy();
    expect(await page.isVisible('#monthly')).toBeTruthy();
    expect(await page.isVisible('#prompt')).toBeTruthy();
    const form = await page.$('kgh-donation');
    const screenshot = await form?.screenshot();
    expect(screenshot).toMatchSnapshot(`test-form-before-click-${browserName}.png`, { threshold: 0.75 });
  });

  test.describe('clicking on $18', () => {
    test.beforeEach(async () => {
      await kgh.pickAmount(18);
    });

    test('shows donation form', async ({ page, browserName }) => {
      const form = await page.$('kgh-donation');
      const { stripeElements } = kgh;
      await stripeElements.waitForAnimations();
      const screenshot = await form?.screenshot();
      expect(screenshot).toMatchSnapshot(`test-form-after-click-${browserName}.png`, { threshold: 0.75 });
    });

    test('sets amount input value', async ({ page }) => {
      expect(await page.$eval('#other-amount', (x: MdOutlinedTextField) => x.value)).toEqual('18');
    });

    test.describe('then filling form', () => {
      test.beforeEach(async () => {
        await kgh.fillForm({
          name: 'A',
          email: 'a.user@bmail.com',
          phone: '555-555-5555',
          message: '',
        });
      });

      test.describe('and submitting', () => {
        test('disables form and sets loading state', async ({ page }) => {
          await page.click('#submit');
          const billing = await page.$('#billing');
          await kgh.updateComplete('#stripe');
          expect(await billing?.getAttribute('loading')).not.toBeNull();
          for (const id of formIds) {
            expect(await page.$eval(`#${id}`, (x: HTMLInputElement) =>
              x.disabled ?? x.hasAttribute('disabled')), id).toBeTruthy();
          }
        });

        test.describe('on response', () => {
          test.beforeEach(async () => {
            await kgh.submitForm();
          });

          test('shows success dialog', async ({ page }) => {
            const response = await kgh.getResponse();
            expect(response).toBeTruthy();
            expect(response?.status).toEqual('succeeded');
            const dialog = await page.$('#success-dialog');
            expect(await dialog?.getAttribute('open')).not.toBeNull();
          });

          test.describe('then closing dialog', function() {
            test.beforeEach(async () => {
              await kgh.closeDialog();
              await sleep(1000);
            });

            test('resets form', async ({ page, browserName }) => {
              const form = await page.$('kgh-donation');
              const response = await kgh.getResponse();
              expect(response).toBeNull();
              const screenshot = await form?.screenshot();
              expect(screenshot).toMatchSnapshot(`test-after-close-success-dialog-${browserName}.png`, { threshold: 0.5 });
            });
          });
        });
      });

      test.describe('setting monthly', () => {
        test.beforeEach(async () => {
          await kgh.setMonthly(true);
        });

        test('shows ongoing checked', async ({ page }) => {
          expect(await page.$eval('#ongoing-fields md-switch', (x: MdSwitch) => x.selected))
            .toBeTruthy();
          expect(await page.isVisible('#ongoing-fields small')).toBeTruthy();
          expect(await page.$eval(
            '#ongoing-fields md-outlined-text-field',
            (x: MdOutlinedTextField) => x.disabled,
          )).toBeTruthy();
        });

        test.describe('clicking ongoing', () => {
          test.beforeEach(() => kgh.setOngoing(false));

          test('shows 24 mo', async ({ page }) => {
            expect(await page.$eval('#ongoing md-switch', (x: MdSwitch) => x.selected))
              .not.toBeTruthy();
            expect(await page.isVisible('#ongoing-fields small')).not.toBeTruthy();
            expect(await page.isVisible('#ongoing-fields md-outlined-text-field')).toBeTruthy();
            expect(await page.$eval(
              '#ongoing-fields md-outlined-text-field',
              (x: MdOutlinedTextField) => x.value,
            ))
              .toEqual('24');
          });

          test.describe('setting 12 mo', () => {
            test.beforeEach(async () => {
              await kgh.setUntil(12);
            });

            test.describe('then submitting', () => {
              test.beforeEach(async () => {
                await kgh.submitForm();
                await sleep(5000);
              });

              test('gets cancelAt response', async () => {
                const error = await kgh.getError();
                expect(error).toBeNull();
                const response = await kgh.getResponse();
                expect(response).not.toBeNull();
                if (!response?.cancelAt)
                  throw new Error('whoops');
                expect(new Date(response.cancelAt).getFullYear())
                  .toEqual(new Date().getFullYear() + 1);
                expect(new Date(response.cancelAt).getUTCMonth())
                  .toEqual(new Date().getUTCMonth());
              });
            });
          });
        });
      });
    });
  });
});
