import type { Page } from '@playwright/test';
import type { KghDonation } from '../src/components/kgh-donation';
import type { LitElement } from 'lit';

import { StripeElements } from './StripeElements.model';

import { sleep } from './sleep';

interface FormModel {
  name: string;
  email: string;
  phone: string;
  message: string;
}

export class KGHPage {
  public static origin =
      process.env.NODE_ENV === 'production' ? 'https://kgh.org.il/'
    : process.env.NODE_ENV === 'staging' ? 'https://kupat-givat-hamivtar-sta-158d1.web.app/'
    : 'http://localhost:5000';

  stripeElements: StripeElements;

  constructor(
    public page: Page,
    public browserName: 'chromium'|'firefox'|'webkit',
    public options?: { log?: boolean }
  ) {
    if (options?.log)
      // eslint-disable-next-line no-console
      page.on('console', msg => console.log(msg.text()));
    this.stripeElements = new StripeElements(page);
  }

  async #setCheckbox(elSel: string, bool: boolean): Promise<void> {
    await this.page.isVisible(elSel);
    await this.updateComplete(elSel);
    const isChecked = await this.page.$eval(elSel, (el: HTMLInputElement) => el.checked);
    if (isChecked === bool)
      // eslint-disable-next-line no-console
      return console.log(`already ${bool}`);
    await this.page.click(elSel, { force: true });
    await this.updateComplete(elSel);
    await this.updateComplete('kgh-donation');
    await sleep(500);
  }

  public async navigate(): Promise<void> {
    await this.page.goto(KGHPage.origin, { waitUntil: 'networkidle' });
  }

  public async pickAmount(amount = 18): Promise<void> {
    await this.page.waitForLoadState('networkidle');
    switch (this.browserName) {
      case 'webkit':
        await this.page.click(`#amount [data-amount=${amount}]`); break;
      default:
        await this.page.click(`#donate-${amount} + label`); break;
    }
    await this.page.waitForSelector('#name', { state: 'visible' });
  }

  public async fillForm(model: FormModel): Promise<void> {
    // eslint-disable-next-line easy-loops/easy-loops
    for (const [key, value] of Object.entries(model)) {
      if (!value) continue;

      const field = await this.page.$(`#${key} input`);
      // on the one hand, playwright-firefox appears to not
      // properly fill the phone input, possibly because of `type="tel"`
      // but on the other hand, setting the value via the DOM doesn't
      // dispatch a `change` event, which our form validation relies on
      if (key === 'phone' && this.browserName === 'firefox') {
        await this.page.$eval(`[name="${key}"]:not(.preset)`, (n: HTMLInputElement, value) => {
          n.value = value;
          n.dispatchEvent(new Event('input'));
        }, value);
      } else {
        await field?.focus();
        await field?.type(value, { delay: 10 });
      }
      await this.page.click('body', { position: { x: 1, y: 1 } });
      await sleep(200);
    }
    await this.stripeElements.fill();
    await this.stripeElements.waitForAnimations();
  }

  public async submitForm(): Promise<void> {
    await this.page.waitForSelector('#submit', { state: 'visible' });
    await this.page.$('#submit', { strict: true });
    await this.page.click('#submit button', { button: 'left', strict: true });
    await this.page.waitForLoadState('networkidle');
    const actionsDiv = await this.page.$('md-dialog [slot="actions"]');
    if (!actionsDiv)
      throw new Error('cant find dialog');
    await actionsDiv.waitForElementState('stable');
    await sleep(5000);
  }

  public async closeDialog(): Promise<void> {
    await this.page.isVisible('#success-dialog [value="ok"]');
    await this.page.click('#success-dialog [value="ok"]');
  }

  public async updateComplete(selector: string) {
    let complete = false;
    do // eslint-disable-line easy-loops/easy-loops
      complete = await this.page.$eval(selector, (el: LitElement) => el.updateComplete);
    while (!complete);
  }

  public async setMonthly(monthly: boolean): Promise<void> {
    await this.#setCheckbox('#monthly md-switch', monthly);
    await sleep(1000);
  }

  public async setOngoing(ongoing: boolean): Promise<void> {
    await this.#setCheckbox('#ongoing md-switch', ongoing);
  }

  public async setUntil(months: number): Promise<void> {
    await this.page.$eval('kgh-donation', (el: KghDonation, x) => el.until = x, months);
    await this.updateComplete('kgh-donation'),
    await this.updateComplete('#ongoing-fields md-outlined-text-field'),
    await sleep(1000);
  }

  async #getProp<S extends keyof KghDonation>(prop: S) {
    await this.page.waitForLoadState('networkidle');
    await this.updateComplete('kgh-donation');
    const val = await this.page.$eval('kgh-donation', (x: KghDonation, prop: any) => x[prop], prop);
    return val as KghDonation[S];
  }

  public async getError(): Promise<KghDonation['error']> {
    return this.#getProp('error');
  }

  public async getResponse(): Promise<KghDonation['response']> {
    return this.#getProp('response');
  }
}
