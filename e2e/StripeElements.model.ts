import type { Page, Frame } from '@playwright/test';

import { sleep } from './sleep';

export class StripeElements {
  private static selectors = {
    cvc: '[name=cvc]',
    cardnumber: '[name=cardnumber]',
    exp: '[name=exp]',
    cardfield: '.CardField-child',
  };

  private frame: Frame;

  private frameInitialized: Promise<void>;

  constructor(public page: Page) {
    page.waitForNavigation().then(() => this.initFrame());
  }

  private async initFrame(): Promise<void> {
    if (this.frameInitialized)
      return await this.frameInitialized;

    let resolved: () => void;

    this.frameInitialized = new Promise(resolve => resolved = resolve);

    const iframe = await this.page.$('.stripe-elements-mount iframe');
    const frame = this.page.frame(await iframe.getAttribute('name'));
    await frame.waitForLoadState('networkidle');
    this.frame = frame;

    resolved();
  }

  public async waitForAnimations(): Promise<void> {
    await this.frameInitialized;
    // const fields = await this.frame.$$(StripeElements.selectors.cardfield);
    // await Promise.all(fields.map(x => x.waitForElementState('stable')));
    await sleep(2000);
  }

  public async waitFor(): Promise<void> {
    await this.frameInitialized;
    await this.waitForAnimations();
  }

  public async fill({
    cardnumber = '4242 4242 4242 4242',
    exp = '12/30',
    cvc = '111',
  } = {}): Promise<void> {
    await this.waitFor();
    await this.frame.type(
      '[name=cardnumber]',
      `${cardnumber} ${exp} ${cvc}`,
      { delay: 50 }
    );
    await this.page.click('body', { position: { x: 1, y: 1 } });
  }
}
