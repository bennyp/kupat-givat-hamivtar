import json from 'eslint-plugin-json';
import noOnlyTests from 'eslint-plugin-no-only-tests';
import js from "@eslint/js";
import ts from "typescript-eslint";
import html from "eslint-plugin-html";
import globals from 'globals';

export default ts.config(
  js.configs.recommended,
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.es6,
        ...globals.fbq,
        ...globals.gtag,
      },
      parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
      },
    },

    settings: {
      litHtmlSources: [
        '@apollo-elements/lit-apollo',
        '@open-wc/testing',
      ],
    },


    plugins: {
      html,
      json,
      'no-only-tests': noOnlyTests,
    },

    rules: {
      'arrow-parens': ['error', 'as-needed',
      ],
      'brace-style': ['error', '1tbs',
        { allowSingleLine: true,
        },
      ],
      'block-spacing': ['error', 'always',
      ],

      'comma-dangle': ['error',
        {
          arrays: 'always-multiline',
          objects: 'always-multiline',
          imports: 'always-multiline',
          exports: 'always-multiline',
          functions: 'ignore',
        },
      ],

      'comma-spacing': 'error',
      'comma-style': ['error', 'last',
      ],
      'curly': ['error', 'multi-or-nest',
      ],
      'eqeqeq': ['error', 'always',
        {
          null: 'ignore',
        },
      ],

      'indent': ['error',
        2,
        {
          flatTernaryExpressions: true,
          SwitchCase: 1,
          ignoredNodes: [
            `ConditionalExpression`,
            `TaggedTemplateExpression[tag.name="html"] > TemplateLiteral CallExpression > ObjectExpression`,
            `TaggedTemplateExpression[tag.name="html"] > TemplateLiteral ObjectExpression`,
            `TaggedTemplateExpression[tag.name="html"] > TemplateLiteral CallExpression > TaggedTemplateLiteral`,
            `TaggedTemplateExpression[tag.name="html"] > TemplateLiteral ArrowFunctionExpression > BlockStatement`,
          ],
        }],

      'linebreak-style': ['error', 'unix',
      ],
      'lines-between-class-members': ['error', 'always',
        { exceptAfterSingleLine: true,
        },
      ],

      'max-len': ['error',
        100,
        {
          ignoreComments: true,
          ignoreTemplateLiterals: true,
          ignorePattern:'^import (type )?\\{? ?\\w+ ?\\}? from \'(.*)\';$',
          ignoreUrls: true,
        },
      ],

      'new-cap': 'off',

      'no-unused-vars': ['warn',
        { ignoreRestSiblings: true,
        },
      ],
      'no-var': 'error',
      'no-console': 'error',
      'no-extend-native': 'error',

      'no-only-tests/no-only-tests': 'error',

      'object-curly-spacing': ['error', 'always',
      ],

      'operator-linebreak': ['error', 'after',
        { 'overrides': { '?': 'after', ':': 'before',
        },
        },
      ],

      'prefer-const': 'error',
      'prefer-destructuring': 'error',
      'prefer-object-spread': 'error',
      'prefer-promise-reject-errors': 'off',
      'prefer-spread': 'error',
      'prefer-template': 'error',

      'require-jsdoc': 'off',

      'spaced-comment': ['error', 'always',
        { markers: ['/',
        ],
        },
      ],
      'space-before-function-paren': ['error',
        { anonymous: 'never', named: 'never', asyncArrow: 'always',
        },
      ],
      'space-infix-ops': 'error',
      'space-unary-ops': 'error',

      'template-tag-spacing': 'error',
      'template-curly-spacing': 'error',
    },
  },
  {
    files: ['**/*.@(test,spec).[jt]s'],
    languageOptions: {
      globals: {
        ...globals.node,
        ...globals.mocha,
      },
    },

    rules: {
      'max-len': 'off',
      'no-invalid-this': 'off',
      'no-console': 'off',
      'require-jsdoc': 'off',
    },
  },
  ts.config({
    files: ['**/*.ts'],
    extends: [
      ...ts.configs.recommended,
      ...ts.configs.stylistic,
      ts.configs.eslintRecommended,
    ],
    languageOptions: {
      parser: ts.parser,
    },
    plugins: {
      '@typescript-eslint': ts.plugin,
    },
    rules: {
      ...ts.configs.recommended.rules,
      'valid-jsdoc': 'off',
      '@typescript-eslint/no-explicit-any': ['warn', {
        ignoreRestArgs: true,
      }],
      '@typescript-eslint/no-unused-vars': ['warn', {
        ignoreRestSiblings: true,
      }],
      '@typescript-eslint/ban-ts-comment': ['warn', {
        'ts-expect-error': 'allow-with-description',
      }],
    },
  }),
  {
    files: [
      '*.cjs.js',
      '.babelrc.js',
      'commitlint.config.?([cm])js',
      'karma.conf.?([cm])js',
      'postcss.config.?([cm])js',
      'rollup.config.?([cm])js',
      'web-*.config.?([cm])js',
      'workbox-config.?([cm])js',
    ],
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
  },
  {
    files: ['*.mjs'],
    languageOptions: {
      parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
      },
    },
  },
  {
    files: ["functions/**/*.ts"],
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
    rules: {
      "no-console": 0,
      "@typescript-eslint/camelcase": 0,
    },
  },
);
