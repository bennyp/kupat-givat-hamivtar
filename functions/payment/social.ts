import Twitter from 'twitter';
import FB from 'facebook-node';

import AMOUNTS from './amounts.json';

interface SocialInput {
  amount: number;
  anonymous: boolean;
  message?: string;
  monthly: boolean;
  name: string;
}

interface FBNodeResponse {
  error: string;
  id: string;
}

FB.setApiVersion('v3.0');

const {
  FACEBOOK_FEED_ID,
  FACEBOOK_TOKEN,
  TWITTER_CONSUMER_KEY,
  TWITTER_CONSUMER_SECRET,
  TWITTER_ACCESS_TOKEN_KEY,
  TWITTER_ACCESS_TOKEN_SECRET,
} = process.env;

const twitter = new Twitter({
  access_token_key: TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: TWITTER_ACCESS_TOKEN_SECRET,
  consumer_key: TWITTER_CONSUMER_KEY,
  consumer_secret: TWITTER_CONSUMER_SECRET,
});

function capitalize(string: string): string {
  return string.replace(/\b\w/g, l => l.toUpperCase());
}

function randomElem<T>(xs: T[]): T {
  return xs[Math.floor(Math.random() * xs.length)] ?? xs[0];
}

function compare(amount: number): string {
  if (amount < 1800)
    return '\n\nתזכו למצוות!';
  else {
    const { prompts } = AMOUNTS.find(x => x.amount <= amount);
    return `That's enough to ${randomElem(prompts)}!\n\nתזכו למצוות!`;
  }
}

export function buildMessage(input: SocialInput): string {
  const { amount, anonymous, message, monthly, name } = input;
  const isShortMessage = !!message && message.length < 180 - (25 - (monthly ? 8 : 0));
  const elaboration = isShortMessage ? ' to Kupat Givat HaMivtar' : '';
  const who = anonymous ? 'Someone anonymously' : capitalize(name).trim();
  const verb = monthly ? 'donates' : 'donated';
  const amt = (amount / 100).toString() + (monthly ? ' monthly' : '') + elaboration;
  const body = message ? message.trim() : compare(amount);
  return `${who} ${verb} $${amt}.\n\n${body}\n\nDonate now: https://kgh.org.il`;
}

async function face(message: string): Promise<FBNodeResponse> {
  return new Promise((resolve, reject) => {
    FB.api(`/${FACEBOOK_FEED_ID}/feed`, 'POST', {
      access_token: FACEBOOK_TOKEN,
      message,
    }, (res: FBNodeResponse) => {
      if (res.error)
        reject(res.error);
      else
        resolve(res);
    });
  });
}

export async function social(input: SocialInput): Promise<void> {
  const status = buildMessage(input);
  console.log('SOCIAL: Attempting to post\n', status);

  try {
    console.log(`TWITTER: Attempting Post for ${input.name}`);
    const response = await twitter.post('statuses/update', { status });
    console.log(`TWITTER: "${response.text}" at ${response.created_at}.`);
  } catch (error) {
    console.log('TWITTER: ', error);
  }

  try {
    console.log(`FACEBOOK: Attempting Post for ${input.name}`);
    const res = await face(status);
    console.log(`FACEBOOK: posted ${res.id}`);
  } catch (error) {
    console.error('FACEBOOK: ', error);
  }
}
