import { stripe } from '../config';

export interface PortalResponse {
  id: string;
  object: string;
  created: number;
  customer: string;
  livemode: boolean;
  return_url: string;
  url: string;
}

export async function getPortal(
  customer: string,
  returnUrl?: string,
): Promise<PortalResponse> {
  return await stripe.billingPortal.sessions.create({
    customer,
    return_url: returnUrl,
  });
}
