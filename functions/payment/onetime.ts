import type { Response } from 'express';
import type { Request } from 'firebase-functions/https';
import type { KghPaymentResponse, KghPaymentRequest } from '../config.js';

import { stripe } from '../config.js';
import { social } from './social.js';

export async function processOnetime(
  _: Request,
  res: Response<string|KghPaymentResponse>,
  body: KghPaymentRequest,
) {
  const source = body.token;
  const { anonymous, monthly, message, name, phone } = body;
  const amount = body.amount * 100;

  const description = `Donation to Kupat Givat HaMivtar EIN 82-2098127`;

  const currency = 'usd';

  const type = anonymous ? 'anonymous' : 'public';

  console.log(`Processing ${type}, one-time $${amount} payment for ${body.name}, ${body.email} ${body.phone}`);

  try {
    const charge = await stripe.charges.create({
      amount,
      receipt_email: body.email,
      currency,
      description,
      source,
      metadata: {
        name,
        phone,
        message,
        anonymous: String(anonymous),
      },
    });

    console.log(`Payment id ${charge.id} succeeded.`);


    social({ amount, anonymous, message, monthly, name })
      .catch(({ code, detail, message }) => {
        console.log(...[code, message, detail].filter(Boolean));
      });

    res.status(200).send({
      amount: charge.amount,
      brand: charge.payment_method_details.card.brand,
      created: charge.created,
      currency: charge.currency,
      description: charge.description,
      last4: charge.payment_method_details.card.last4,
      metadata: charge.metadata,
      name: charge.billing_details.name,
      monthly,
      anonymous,
      paid: charge.paid,
      receiptUrl: charge.receipt_url,
      status: charge.status,
    });
  } catch (error) {
    console.log('STRIPE CHARGE ERROR', error.type, error.message);
    switch (error.type) {
      case 'StripeCardError':
      case 'RateLimitError':
      case 'StripeAPIError':
        res.status(400).send(error.message);
        break;
      case 'StripeInvalidRequestError':
      case 'StripeConnectionError':
      case 'StripeAuthenticationError':
      default:
        res.status(400).send('An Error Occured. Please try again later.');
        break;
    }
  }
}
