/* eslint-disable camelcase */

import type Stripe from 'stripe';

import type { Response } from 'express';
import { type Request } from 'firebase-functions/https';

import { KghPaymentResponse, KghPaymentRequest, stripe, firestore } from '../config';
import { getPortal } from './portal';

import { social } from './social';

import { addMonths, getUnixTime, fromUnixTime } from 'date-fns';

interface PriceOpts {
  amount: number;
  currency: string;
}

async function createCustomer(body: KghPaymentRequest): Promise<Stripe.Customer> {
  const { name, phone, email, token: source } = body;
  const customer = await stripe.customers.create({ name, phone, email, source });
  await firestore.collection('donors').doc(email).set(customer);
  return customer;
}

async function createPrice(opts: PriceOpts): Promise<Stripe.Price> {
  const currency = opts?.currency ?? 'usd';
  const unit_amount = opts?.amount ?? 0;
  const product = process.env.MONTHLY_PRODUCT_ID;
  const recurring = { interval: 'month' as const };
  const price = await stripe.prices.create({ unit_amount, currency, recurring, product });
  await firestore.collection('prices').doc(`${unit_amount}-${currency}`).set(price);
  return price;
}

export async function processMonthly(
  _: Request,
  res: Response,
  body: KghPaymentRequest,
): Promise<void|Response<KghPaymentResponse>> {
  const description = `Donation to Kupat Givat HaMivtar EIN 82-2098127`;
  const { anonymous, message, name, phone, email, token: source, until } = body;
  const amount = body.amount * 100;

  const currency = 'usd';

  const type = anonymous ? 'anonymous' : 'public';

  console.log(`Processing ${type}, monthly $${body.amount} payment for ${name}, ${email} ${phone}`);

  const stripeCustomerRef = (await firestore.collection('donors').doc(email).get());
  const stripeCustomerDoc = stripeCustomerRef.data();
  const stripeCustomerId = stripeCustomerDoc?.id;

  const stripePriceRef = await firestore.collection('prices').doc(`${amount}-${currency}`).get();
  const stripePriceDoc = stripePriceRef.data();
  const stripePriceId = stripePriceDoc?.id;

  const stripeCustomer =
      !stripeCustomerId ? await createCustomer(body)
    : await stripe.customers.retrieve(stripeCustomerId);

  const stripePrice =
      !stripePriceId ? await createPrice({ amount, currency })
    : await stripe.prices.retrieve(stripePriceId);

  console.log(`Stripe Customer ${stripeCustomer.id}`);

  try {
    try {
      await stripe.customers.update(stripeCustomer.id, { source }) as Stripe.Customer;
    } catch (error) {
      if (error.code !== 'token_already_used')
        throw error;
    }

    const cancel_at = getUnixTime(addMonths(new Date, Number(until)));

    const subscription = await stripe.subscriptions.create({
      customer: stripeCustomer.id,
      items: [{ price: stripePrice.id }],
      metadata: { name, phone, message, anonymous: String(anonymous) },
      ...until && { cancel_at },
    });

    console.log(`Created subscription ${subscription.id}. Status ${subscription.status}. Cancels at ${subscription.cancel_at}`);

    if (subscription.status === 'incomplete')
      return res.redirect((await getPortal(stripeCustomer.id)).url);
    else {
      const [item] = subscription?.items?.data ?? [];
      const amount = item?.price.unit_amount;
      const monthly = true;
      const cancelAt =
          !subscription.cancel_at ? 0
        : fromUnixTime(subscription.cancel_at).toISOString();


      social({ amount, anonymous, message, monthly, name })
        .catch(({ code, detail, message }) => {
          console.log(...[code, message, detail].filter(Boolean));
        });

      res.status(200).send({
        amount,
        created: subscription.created,
        currency: item?.price?.currency,
        description,
        metadata: subscription.metadata,
        name: (stripeCustomer as Stripe.Customer).name,
        cancelAt,
        monthly,
        anonymous,
        status: subscription.status,
      } as KghPaymentResponse);
    }
  } catch (error) {
    console.error(error);
    console.log('STRIPE SUBSCRIPTION ERROR', error.type, error.message);
    switch (error.type) {
      case 'StripeCardError':
      case 'RateLimitError':
      case 'StripeAPIError':
        res.status(400).send(error.message);
        break;
      case 'StripeInvalidRequestError':
      case 'StripeConnectionError':
      case 'StripeAuthenticationError':
      default:
        res.status(400).send('An Error Occured. Please try again later.');
        break;
    }
  }
}
