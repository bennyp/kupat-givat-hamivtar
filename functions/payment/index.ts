import type { Response } from 'express';
import { HttpsError, type Request } from 'firebase-functions/https';
import { KghPaymentRequest, app, limiter } from '../config';
import { processMonthly } from './monthly.js';
import { processOnetime } from './onetime.js';
import { getPortal } from './portal.js';
import { getAppCheck } from 'firebase-admin/app-check';

async function verifyAppCheckHeader(req: Request) {
  const token = req.header('X-Firebase-AppCheck') || req.header('x-firebase-appcheck');
  if (!token) {
    console.log(req);
    console.error('No AppCheck Header');
    return false;
  } else {
    try {
      // const claims = await getAppCheck(app).verifyToken(token, { consume: true });
      // console.log(claims);
      // return !claims.alreadyConsumed;
      const claims = await getAppCheck(app).verifyToken(token);
      console.log(claims);
      return claims.token;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}

export async function paymentFunction(
  req: Request,
  res: Response
) {
  const body = JSON.parse(req.body) as KghPaymentRequest;
  // Rate limit based on user IP
  if (process.env.NODE_ENV === 'production') {
    const appCheckToken = await verifyAppCheckHeader(req);
    if (!appCheckToken)
      throw new HttpsError('unauthenticated', 'Unauthorized');
    else {
      const cfIp = req.header('cf-connecting-ip');
      const { ip } = req;
      try {
        await limiter.rejectOnQuotaExceededOrRecordUsage(cfIp ?? ip);
      } catch (error) {
        throw new HttpsError(
          'resource-exhausted',
          'Try again in a few minutes',
          { error, cfIp, ip },
        );
      }
    }
  }

  if (body.monthly)
    await processMonthly(req, res, body);
  else
    await processOnetime(req, res, body);
}

export async function portalFunction(
  _: Request,
  res: Response,
) {
  const customer = '';
  const portal = await getPortal(customer);
  res.set(200).send(portal);
}
