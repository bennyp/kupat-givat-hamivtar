import type { ErrorRequestHandler } from 'express';

import { auth } from 'firebase-admin';
import { getUserByClaims } from './users-helpers';
import { wrap, formatReq, getSessionCookie, SESSION_COOKIE_NAME } from './express-helpers';

export const ErrorHandlers: Record<string, ErrorRequestHandler> = {
  logErrors(error, req, res) {
    console.error(error, '\n', formatReq(req));
    res.redirect('/gabbaim/login/');
  },
};

export const Middlewares = wrap({

  logger(req, _res, next) {
    console.info(formatReq(req));
    next();
  },

  async guard(req, res, next) {
    const { user } = req;

    const roles = user?.roles ?? [];

    if (!user || !roles.includes('gabbai'))
      return res.redirect('/gabbaim/login');
    else
      next();
  },

  async session(req, _res, next) {
    const sessionCookie = getSessionCookie(req);

    if (sessionCookie) {
      const claims = await auth().verifySessionCookie(sessionCookie, true);
      req.user = await getUserByClaims(claims);
    }
    return next();
  },

  /**
   * Create the session cookie. This will also verify the ID token in the process.
   * The session cookie will have the same claims as the ID token.
   * To only allow session cookie setting on recent sign-in, auth_time in ID token
   * can be checked to ensure user was recently signed in before creating a session cookie.
   */
  async login(req, res) {
    const { idToken } = req.body;

    // Set session expiration to 5 days.
    const expiresIn = 5 * 24 * 60 * 60 * 1000;
    const cookie = await auth().createSessionCookie(idToken, { expiresIn });

    return res
      .cookie(SESSION_COOKIE_NAME, cookie, { maxAge: expiresIn, httpOnly: true, secure: true })
      .send('OK');
  },

});
