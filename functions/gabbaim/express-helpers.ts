import type { Handler, Request } from 'express';

type Handlers = Record<string, Handler>;

/**
 * IMPORTANT! Must use this session cookie name.
 * @see https://firebase.google.com/docs/hosting/manage-cache#using_cookies
 */
export const SESSION_COOKIE_NAME = '__session';

export function getSessionCookie(req: Request): string {
  return req.cookies[SESSION_COOKIE_NAME];
}

export function formatReq(req: Request): string {
  const cookie = `Session: ${getSessionCookie(req) ? '✅' : '❌'}`;
  const user = `User: ${req.user ? `${req.user.name}; Roles: ${req.user.roles.join(', ')}` : '❌'}`;
  return `${req.method.toUpperCase()} ${req.path} (${cookie}, ${user})`;
}

export function wrap(middlewares: Handlers): Handlers {
  return Object.fromEntries(
    Object.entries(middlewares)
      .map(([key, method]) =>
        [key, ((req, res, next) =>
          Promise.resolve(method(req, res, next)).catch(next)) as Handler])
  );
}
