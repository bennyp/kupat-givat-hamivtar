import { auth } from 'firebase-admin';
import { getCampaignDoc, getCampaigns } from '../campaigns';
import { getSessionCookie, wrap } from './express-helpers';
import { getStripeDonationsDuringCampaign } from '../campaigns';

export const Routes = wrap({

  async login(req, res) {
    if (req.user)
      return res.redirect('/gabbaim/');
    else
      return res.render('login.html', { csrfToken: req.csrfToken() });
  },

  async logout(req, res) {
    try {
      const sessionCookie = getSessionCookie(req) || '';
      res.clearCookie('session');
      const claims = await auth().verifySessionCookie(sessionCookie);
      await auth().revokeRefreshTokens(claims.sub);
    } finally {
      res.redirect('/gabbaim/');
    }
  },

  async update(req, res) {
    const { donations } = req.body;

    const doc = await getCampaignDoc('purim-2021');

    const totalNonStripeUSD = parseFloat(donations);

    await doc.ref.update({ totalNonStripeUSD });

    return res.redirect('/gabbaim/');
  },

  async main(req, res) {
    const _campaigns = await getCampaigns();
    const campaigns = await Promise.all(_campaigns.map(async campaign => {
      const stripeDonations = await getStripeDonationsDuringCampaign(campaign);
      return { ...campaign, stripeDonations };
    }));
    return res.status(200).render('gabbaim.html', {
      csrfToken: req.csrfToken(),
      campaigns,
      user: req.user,
    });
  },

});
