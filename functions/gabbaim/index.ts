import express from 'express';
import cookieParser from 'cookie-parser';
import csurf from 'csurf';
import cors from 'cors';

import path from 'path';
import nunjucks from 'nunjucks';
import { format } from 'date-fns';

import { ErrorHandlers, Middlewares } from './middlewares';
import { Routes } from './routes';
import { User } from './users-helpers';

declare global {
  // eslint-disable-next-line
  namespace Express {
    interface Request {
      user: User;
    }
  }
}

const { NODE_ENV = 'development' } = process.env;

const app = express();

const VIEWS_PATH = path.join(__dirname, './views');

nunjucks.configure(VIEWS_PATH, { autoescape: true, express: app })
  .addGlobal('$env', NODE_ENV || 'development')
  .addFilter('formatDate', function formatDate(date: string, tokens = 'MMMM dd, yyyy hh:mm') {
    return format(new Date(date), tokens);
  })
  .addFilter('sumAmount', function sumAmount(list: { amount: number }[]) {
    return list.reduce((sum, { amount }) => sum + amount, 0);
  })
  .addFilter('formatCurrency', function formatCurrency(amount: number, currency = 'USD') {
    const formatter = new Intl.NumberFormat('en-US', { currency, style: 'currency' });
    return formatter.format(amount);
  });

export const gabbaimApp = app
  .set('view engine', 'html')
  .use(cookieParser())
  .use(cors({ credentials: true }))
  .use(csurf({ cookie: true }))
  .use(Middlewares.session)
  .use(Middlewares.logger)
  .get('/gabbaim', Middlewares.guard, Routes.main)
  .get('/gabbaim/login', Routes.login)
  .get('/gabbaim/logout', Routes.logout)
  .post('/gabbaim/login', express.json(), Middlewares.login)
  .post('/gabbaim/update', Middlewares.guard, express.urlencoded({ extended: true }), Routes.update)
  .use(ErrorHandlers.logErrors);
