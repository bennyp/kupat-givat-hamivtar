import { firestore, auth } from 'firebase-admin';

export interface User {
  uid: string;
  email: string;
  name?: string;
  salutation?: string;
  roles?: string[];
}

async function addUidToUserByEmail(email: string, uid: string): Promise<void> {
  const [doc] = (
    await firestore()
      .collection('users')
      .where('email', '==', email)
      .get()
  ).docs;

  await doc.ref.update({ uid });
}

async function getUserDataByEmail(email: string): Promise<FirebaseFirestore.DocumentData> {
  const [doc] = (
    await firestore()
      .collection('users')
      .where('email', '==', email)
      .get()
  ).docs;
  return doc?.data?.() ?? null;
}

export async function getUserByClaims(
  claims: auth.DecodedIdToken
): Promise<User> {
  const { email, uid, name } = claims;
  const data = await getUserDataByEmail(email);
  if (!data?.uid)
    addUidToUserByEmail(email, uid);
  const { title, roles } = await getUserDataByEmail(email);
  const salutation = title && `${title} ${name.split(' ').pop()}`;
  return { email, uid, salutation, roles, name };
}
