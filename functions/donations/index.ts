import type { Response } from 'express';
import type { Request } from 'firebase-functions/https';

import { getStripeDonationsDuringCampaign } from '../campaigns';
import { getCampaignById } from '../campaigns';

type QueryParam = Request['query'][string];

function ensureString(date: QueryParam, param: string): string|undefined {
  if (date) {
    if (Array.isArray(date))
      throw new Error(`${param} cannot be an array`);

    if (typeof date !== 'string')
      throw new Error(`${param} must be a string`);

    return date;
  }
}

export async function donationsFunction(
  req: Request,
  res: Response,
) {
  console.log(req);
  try {
    const sumOnly = req.query['sum-only'] === 'true';

    const campaignId = ensureString(req.query['campaign-id'], 'campaign-id');

    const campaign = await getCampaignById(campaignId ?? 'purim-2021')
      .catch(error => {
        console.error(error);
        throw new Error('Unrecognized campaign');
      });

    const { goalUSD, totalNonStripeUSD = 0 } = campaign;

    const stripeDonations = await getStripeDonationsDuringCampaign(campaign)
      .catch(error => {
        console.error(error);
        throw new Error('Unexpected Error');
      });

    // NB: not accounting for currency
    const sum = stripeDonations.reduce((tally, donation) =>
      tally + donation.amount, totalNonStripeUSD * 100);

    const donations = sumOnly ? undefined : stripeDonations;

    res.status(200).send({ donations, sum, goalUSD });
  } catch (error) {
    res.status(500).send(error.message);
  }
}
