import dotenv from 'dotenv';
dotenv.config({ path: '../.env' });

import { initializeApp } from 'firebase-admin/app';
import { getFirestore } from 'firebase-admin/firestore';

import Stripe from 'stripe';
import { FirebaseFunctionsRateLimiter } from '@pwrs/firebase-functions-rate-limiter';

export interface KghDonationsRequest {
  /** ISO Date string */
  startDate: string;
  /** ISO Date string */
  endDate: string;
  /** when true, just return the sum donated. */
  sumOnly: boolean;
}

interface AnonymousCharge {
  amount: number;
  currency: Stripe.Charge['currency'];
  created: Stripe.Charge['created'];
}

export interface KghDonationsResponse {
  donations?: AnonymousCharge[];
  goalUSD: number;
  sum: number;
}

export interface KghPaymentRequest {
  amount: number;
  anonymous: boolean;
  email: string;
  message?: string;
  monthly: boolean;
  until: string;
  name: string;
  phone: string;
  token: string;
}

export interface KghPaymentResponse {
  amount: number;
  brand: Stripe.Card['brand'];
  created: number;
  currency: Stripe.Card['currency'];
  description: string;
  error?: Error;
  last4: string;
  monthly: boolean;
  anonymous: boolean;
  name: string;
  cancelAt?: string;
  paid: boolean;
  receiptUrl?: string;
  status: string;
  metadata: Stripe.Metadata;
}

export const app = initializeApp();

export const firestore = getFirestore();

firestore.settings({ timestampsInSnapshots: true });

export const limiter =
  FirebaseFunctionsRateLimiter.withFirestoreBackend({
    name: 'payment-rate-limiter',
    periodSeconds: 60,
    maxCalls: 1,
    debug: process.env.NODE_ENV !== 'production',
  }, firestore);

export const stripe = new Stripe(process.env.STRIPE_KEY, {
  // @ts-expect-error: not ready to migrate yet
  apiVersion: '2020-08-27',
});
