import dotenv from 'dotenv';
dotenv.config();

import configTest from 'firebase-functions-test';

const projectId = process.env.FIREBASE_PROJECT || 'kupat-givat-hamivtar-sta-158d1';

export const test = configTest({
  databaseURL: `https://${projectId}.firebaseio.com`,
  storageBucket: `${projectId}.appspot.com`,
  projectId: `${projectId}`,
}, './service-account.json');
