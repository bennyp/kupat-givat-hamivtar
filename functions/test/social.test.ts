import test from 'tape';

import { buildMessage } from '../payment/social';

import BUILD_MESSAGE_CASES from './buildMessage.cases.json';

test('buildMessage', function(assert) {
  assert.plan(BUILD_MESSAGE_CASES.length);
  for (const { description, input, expected } of BUILD_MESSAGE_CASES)
    assert.equal(buildMessage(input), expected, description);
});
