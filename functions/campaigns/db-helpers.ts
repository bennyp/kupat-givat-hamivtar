import type { Campaign } from './types';

import { firestore } from 'firebase-admin';

export async function getCampaignDoc(
  id: string
): Promise<FirebaseFirestore.QueryDocumentSnapshot<Campaign>> {
  const [doc] = (
    await firestore()
      .collection('promotions')
      .where('id', '==', id)
      .get()
  ).docs;
  return doc as FirebaseFirestore.QueryDocumentSnapshot<Campaign>;
}

export async function getCampaigns(): Promise<Campaign[]> {
  const { docs } = await firestore()
    .collection('promotions')
    .orderBy('endDate', 'desc')
    .get();

  return docs.map(doc => doc.data()) as Campaign[];
}

export async function getCampaignById(id: string): Promise<Campaign> {
  const doc = await getCampaignDoc(id);
  return doc.data() as Campaign;
}
