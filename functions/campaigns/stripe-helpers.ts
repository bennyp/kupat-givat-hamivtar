import type { Campaign } from '../campaigns';

import { stripe } from '../config';

import type Stripe from 'stripe';

interface Donation {
  amount: number;
  currency: string;
  created: number;
}

async function getCharges({ gte, lte, startingAfter, endingBefore }) {
  return await stripe.charges.list({
    limit: 100,
    ...startingAfter && { starting_after: startingAfter },
    ...endingBefore && { ending_before_after: endingBefore },
    ...(gte || lte) && {
      created: {
        ...gte && { gte },
        ...lte && { lte },
      },
    },
  });
}

function roundDate(
  dateString: string,
  param: string,
  rounding: 'round'|'ceil'|'floor' = 'round'
): number {
  try {
    return Math[rounding](new Date(dateString).getTime() / 1000);
  } catch {
    throw new Error(`${param} must be an ISO date string`);
  }
}

function pick({ amount, currency, created }) {
  return { amount, currency, created };
}

export async function getStripeDonationsDuringCampaign(campaign: Campaign): Promise<Donation[]> {
  const { startDate, endDate } = campaign;

  const gte = roundDate(startDate, 'start-date', 'floor');

  const lte = roundDate(endDate, 'end-date', 'ceil');

  let startingAfter: string;
  let endingBefore: string;
  let charges: Stripe.Charge[] = [];
  let response: Stripe.ApiList<Stripe.Charge>;

  // eslint-disable-next-line easy-loops/easy-loops
  do {
    response = await getCharges({ gte, lte, startingAfter, endingBefore });
    startingAfter = response.data[response.data.length - 1]?.id;
    charges = charges.concat(response.data);
  } while (response.has_more);

  return charges.map(pick);
}
