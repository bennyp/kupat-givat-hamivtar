export interface Campaign {
  /** Promotion id */
  id: string;
  /** Promotion start date. ISO 8601 Date Format String. */
  startDate: string;
  /** Promotion end date. ISO 8601 Date Format String. */
  endDate: string;
  /** Promotion goal, in whole US Dollars. */
  goalUSD: number;
  /** Customer-facing, human-readable promotion Name.  */
  name: string;
  /** Whole US dollars (equivalent) donated so far, asides from Stripe donations. */
  totalNonStripeUSD: number;
}
