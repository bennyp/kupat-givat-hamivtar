import './config.js'; // make sure to do this first
import { onRequest } from 'firebase-functions/https';
import { paymentFunction, portalFunction } from './payment/index.js';
import { donationsFunction } from './donations/index.js';
import { gabbaimApp } from './gabbaim/index.js';

export const donations = onRequest({ cors: true }, donationsFunction);
export const payment = onRequest({ cors: true }, paymentFunction);
export const portal = onRequest({ cors: true }, portalFunction);
export const gabbaim = onRequest({ cors: true }, gabbaimApp);
