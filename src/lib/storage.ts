export const get = (key: string): unknown => {
  let stored = '';
  try {
    stored = localStorage.getItem(key);
    return JSON.parse(stored);
  } catch (e) {
    return undefined;
  }
};

export const set = (key: string, value: unknown): void =>
  localStorage.setItem(key, JSON.stringify(value || ''));
