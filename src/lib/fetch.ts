export const handleAsJson = (response: Response): Promise<unknown> => response.json();
export const handleAsText = (response: Response): Promise<string> => response.text();
export const handleAsBlob = (response: Response): Promise<Blob> => response.blob();
