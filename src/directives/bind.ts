import { directive, ElementPart, Part, PartType } from 'lit/directive.js';
import { AsyncDirective } from 'lit/async-directive.js';
import { noChange, ReactiveElement } from 'lit';

function isElementPart(partInfo: Part): partInfo is ElementPart {
  return partInfo.type === PartType.ELEMENT;
}

class BindDirective extends AsyncDirective {
  partInfo: ElementPart;
  element: HTMLInputElement & ReactiveElement;
  host: ReactiveElement & { loading?: boolean; };
  propertyName: string;
  private type: string;
  private isBoolean = false;

  constructor(partInfo: Part) {
    super(partInfo);
    if (!isElementPart(partInfo))
      throw new Error('bind() directive must be used in element position.');
    if (!(partInfo.element as HTMLElement).dataset.model)
      throw new Error('bind() directive must be used on an element with a data-model attribute.');
    this.partInfo = partInfo as this['partInfo'];
    this.element = partInfo.element as this['element'];
    this.isBoolean = 'checked' in this.element;
    this.host = partInfo.options.host as this['host'];
    this.propertyName = this.element.dataset.model;
    this.type = this.element.dataset.changeEvent ?? this.isBoolean ? 'change' : 'input';
    this.host.updateComplete.then(() => {
      if (this.element.isConnected)
        this.reconnected();
    });
  }

  /* eslint-disable no-invalid-this */
  private onChange = async () => {
    if (this.element.value === undefined)
      return;

    const value =
        this.isBoolean ? this.element.checked
      : this.element.type === 'number' ? Number(this.element.value)
      : this.element.value;

    await new Promise(r => requestAnimationFrame(r));

    await this.element.updateComplete;
    await this.host.updateComplete;

    this.host[this.propertyName] = value;
  }
  /* eslint-enable no-invalid-this */

  reconnected() {
    this.partInfo.element.addEventListener(this.type, this.onChange);
  }

  disconnected() {
    this.partInfo.element.removeEventListener(this.type, this.onChange);
  }

  render(opts?: { disabled?: boolean; hidden?: boolean; value?: string|number }) {
    this.element.disabled = this.host.loading || (opts?.disabled ?? false);
    // NB: no ?. here because if user opts-in, we clobber template's `hidden`
    if (opts && opts.hidden !== undefined)
      this.element.hidden = opts.hidden;
    this.element.name = this.propertyName;
    if (this.isBoolean)
      this.element.checked = this.host[this.propertyName];
    else
      this.element.value = (opts?.value ?? this.host[this.propertyName]);
    return noChange;
  }
}

export const bind = directive(BindDirective);
