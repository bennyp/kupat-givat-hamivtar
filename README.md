# define-element

Defines a Custom Element from a name and template

## Properties

| Property | Attribute | Type     | Description                    |
|----------|-----------|----------|--------------------------------|
| `name`   | `name`    | `string` | the name of the custom-element |
