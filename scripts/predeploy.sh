if [ -z "$CI_ENVIRONMENT_NAME" ]; then
  # WORKAROUND for https://github.com/Jblew/firebase-functions-rate-limiter/issues/42
  rm -rf functions/node_modules/firebase-functions-rate-limiter/node_modules/@google-cloud/
  npm run build --prefix functions;
fi
