// @ts-check
import dotenv from 'dotenv';
dotenv.config({ path: [
  '.env',
  '.env.firebase',
]});

import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';

// import minifyLiterals from 'rollup-plugin-minify-html-literals';
import litcss from 'rollup-plugin-lit-css';
import esbuild from 'rollup-plugin-esbuild';
import html from '@open-wc/rollup-plugin-html';

import { copy } from '@web/rollup-plugin-copy';

const {
  ROLLUP_WATCH,
  NODE_ENV,
  STRIPE_LIVE_KEY,
  STRIPE_TEST_KEY,
  RECAPTCHA_SITE_KEY,
  RECAPTCHA_ENTERPRISE_SITE_KEY,
  FIREBASE_API_KEY,
  FIREBASE_APP_ID,
  FIREBASE_PROJECT_ID,
  FIREBASE_APPCHECK_DEBUG_TOKEN = 'false',
} = process.env;

if (!FIREBASE_API_KEY) throw new Error('Missing FIREBASE_API_KEY')
if (!FIREBASE_APP_ID) throw new Error('Missing FIREBASE_APP_ID')
if (!FIREBASE_PROJECT_ID) throw new Error('Missing FIREBASE_PROJECT_ID')
if (!RECAPTCHA_SITE_KEY) throw new Error('Missing RECAPTCHA_SITE_KEY')
if (!RECAPTCHA_ENTERPRISE_SITE_KEY) throw new Error('Missing RECAPTCHA_ENTERPRISE_SITE_KEY')

const production = NODE_ENV === 'production' || !ROLLUP_WATCH;
const staging = NODE_ENV === 'staging';

const STRIPE_PUBLISHABLE_KEY = (NODE_ENV === 'production') ? STRIPE_LIVE_KEY : STRIPE_TEST_KEY;

if (!STRIPE_PUBLISHABLE_KEY) throw new Error('Missing STRIPE_PUBLISHABLE_KEY')

try {
  const partialKey = STRIPE_PUBLISHABLE_KEY?.substring(0, 7);
  // eslint-disable-next-line no-console
  console.log('STRIPE KEY:', partialKey);
} catch (e) {
  if (e instanceof TypeError && e.message.startsWith('Cannot read'))
    throw new Error('Could not find publishable key. Check environment variables');
  else
    throw e;
}

export default /** @type {import('rollup').RollupOptions} */ ({
  input: 'src/index.html',
  treeshake: !!production,
  output: {
    dir: 'public',
    format: 'esm',
    sourcemap: true,
  },
  plugins: [
    resolve(),
    litcss(),
    html({ minify: production }),
    replace({
      preventAssignment: true,
      values: {
        FIREBASE_API_KEY,
        FIREBASE_APP_ID,
        FIREBASE_PROJECT_ID,
        RECAPTCHA_SITE_KEY,
        RECAPTCHA_ENTERPRISE_SITE_KEY,
        STRIPE_PUBLISHABLE_KEY,
        __FIREBASE_APPCHECK_DEBUG_TOKEN: production || staging ? 'false' : FIREBASE_APPCHECK_DEBUG_TOKEN,
      },
    }),
    copy({
      patterns: '**/*.{png,svg,jpg,json,css}',
      rootDir: './src',
      exclude: [],
    }),
    esbuild({ minify: production, sourceMap: true }),
    ...!production ? [] : [
      // minifyLiterals(),
    ],
  ],
});
